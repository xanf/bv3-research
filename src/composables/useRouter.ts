import { computed, getCurrentInstance } from 'vue'

const ANCHOR_TAG = 'a'
// TODO: improve types
export function useRouter() {
  const components = getCurrentInstance()?.appContext.components || {}
  const hasRouter = 'RouterLink' in components
  const hasNuxt = 'NuxtLink' in components

  return computed(() => ({
    hasRouter: hasRouter || hasNuxt,
    defaultLinkComponent: hasNuxt ? 'nuxt-link' : hasRouter ? 'router-link' : ANCHOR_TAG,
  }))
}
