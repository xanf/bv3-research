import { getCurrentInstance, computed } from 'vue'

export function useLegacyVModel<T extends Record<string, unknown>>(
  props: T,
  propName = 'value',
  eventName = 'input'
) {
  const instance = getCurrentInstance()
  if (!instance) {
    throw new Error('instance is missing')
  }

  const modelValue = computed(() => (props['modelValue'] || props[propName]) as T['modelValue'])
  const updateModelValue = (value: T['modelValue']) => {
    instance.emit('update:modelValue', value)
    instance.emit(eventName, value)
  }

  return {
    value: modelValue,
    updateModelValue,
  }
}
