import { createRouter, createMemoryHistory } from 'vue-router'
import { mount } from '@vue/test-utils'
import { describe, it, expect } from 'vitest'
import BLink from './BLink.vue'

describe('b-link', () => {
  describe('router-link support', () => {
    it('works', async () => {
      const router = createRouter({
        history: createMemoryHistory(),
        routes: [
          { path: '/', component: { name: 'R', template: '<div class="r">ROOT</div>' } },
          { path: '/a', component: { name: 'A', template: '<div class="a">A</div>' } },
          { path: '/b', component: { name: 'B', template: '<div class="a">B</div>' } },
        ],
      })

      // Fake Gridsome `<g-link>` component
      const GLink = {
        name: 'GLink',
        props: {
          to: {
            type: [String, Object],
            default: '',
          },
        },
        template: '<a :href="to"><slot></slot></a>',
      }

      const App = {
        router,
        components: { BLink },
        template: `<main>
          <!-- router-link -->
          <b-link to="/a">to-a</b-link>
          <!-- regular link -->
          <b-link href="/a">href-a</b-link>
          <!-- router-link -->
          <b-link :to="{ path: '/b' }">to-path-b</b-link>
          <!-- g-link -->
          <b-link to="/a" router-component-name="g-link">g-link-a</b-link>
          <router-view />
        </main>`,
      }

      const wrapper = mount(App, {
        global: {
          plugins: [router],
          components: {
            GLink,
          },
        },
        attachTo: document.body,
      })

      expect(wrapper.vm).toBeDefined()
      expect(wrapper.element.tagName).toBe('MAIN')

      expect(wrapper.findAll('a').length).toBe(4)

      const $links = wrapper.findAllComponents<typeof BLink>('a')
      $links.forEach(($link) => {
        expect($link.vm).toBeDefined()
        expect($link.findComponent(BLink).vm).toBe($link.vm)
      })
      expect(
        $links.map(($link) => $link.findComponent({ name: 'RouterLink' }).exists())
      ).toStrictEqual([true, false, true, false])

      expect($links[3]?.findComponent(GLink).exists()).toBe(true)
    })
  })
})
