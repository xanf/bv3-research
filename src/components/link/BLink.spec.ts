import { mount } from '@vue/test-utils'
import { describe, it, expect, vi } from 'vitest'
import BLink from './BLink.vue'
import { defaultEventHub } from '../../eventHub/eventHub'

// Vue.use(VueRouter)

describe('b-link', () => {
  it('has expected default structure', async () => {
    const wrapper = mount(BLink)

    expect(wrapper.element.tagName).toBe('A')
    expect(wrapper.attributes('href')).toEqual('#')
    expect(wrapper.attributes('target')).toEqual('_self')
    expect(wrapper.attributes('rel')).toBeUndefined()
    expect(wrapper.attributes('aria-disabled')).toBeUndefined()
    expect(wrapper.classes().length).toBe(0)
    expect(wrapper.text()).toEqual('')
  })

  it('renders content from default slot', async () => {
    const wrapper = mount(BLink, {
      slots: {
        default: 'foobar',
      },
    })

    expect(wrapper.element.tagName).toBe('A')
    expect(wrapper.attributes('href')).toEqual('#')
    expect(wrapper.attributes('target')).toEqual('_self')
    expect(wrapper.attributes('rel')).toBeUndefined()
    expect(wrapper.attributes('aria-disabled')).toBeUndefined()
    expect(wrapper.classes().length).toBe(0)
    expect(wrapper.text()).toEqual('foobar')
  })

  it('sets attribute href to user supplied value', async () => {
    const wrapper = mount(BLink, {
      props: {
        href: '/foobar',
      },
    })

    expect(wrapper.element.tagName).toBe('A')
    expect(wrapper.attributes('href')).toEqual('/foobar')
    expect(wrapper.attributes('target')).toEqual('_self')
    expect(wrapper.attributes('rel')).toBeUndefined()
    expect(wrapper.attributes('aria-disabled')).toBeUndefined()
    expect(wrapper.classes().length).toBe(0)
    expect(wrapper.text()).toEqual('')
  })

  it('sets attribute href when user supplied href is hash target', async () => {
    const wrapper = mount(BLink, {
      props: {
        href: '#foobar',
      },
    })

    expect(wrapper.element.tagName).toBe('A')
    expect(wrapper.attributes('href')).toEqual('#foobar')
    expect(wrapper.attributes('target')).toEqual('_self')
    expect(wrapper.attributes('rel')).toBeUndefined()
    expect(wrapper.attributes('aria-disabled')).toBeUndefined()
    expect(wrapper.classes().length).toBe(0)
    expect(wrapper.text()).toEqual('')
  })

  it('should set href to string `to` prop', async () => {
    const wrapper = mount(BLink, {
      props: {
        to: '/foobar',
      },
    })

    expect(wrapper.element.tagName).toBe('A')
    expect(wrapper.attributes('href')).toEqual('/foobar')
    expect(wrapper.attributes('target')).toEqual('_self')
    expect(wrapper.attributes('rel')).toBeUndefined()
    expect(wrapper.attributes('aria-disabled')).toBeUndefined()
    expect(wrapper.classes().length).toBe(0)
    expect(wrapper.text()).toEqual('')
  })

  it('should set href to path from `to` prop', async () => {
    const wrapper = mount(BLink, {
      props: {
        to: { path: '/foobar' },
      },
    })

    expect(wrapper.element.tagName).toBe('A')
    expect(wrapper.attributes('href')).toEqual('/foobar')
    expect(wrapper.attributes('target')).toEqual('_self')
    expect(wrapper.attributes('rel')).toBeUndefined()
    expect(wrapper.attributes('aria-disabled')).toBeUndefined()
    expect(wrapper.classes().length).toBe(0)
    expect(wrapper.text()).toEqual('')
  })

  it('should default rel to `noopener` when target==="_blank"', async () => {
    const wrapper = mount(BLink, {
      props: {
        href: '/foobar',
        target: '_blank',
      },
    })

    expect(wrapper.element.tagName).toBe('A')
    expect(wrapper.attributes('href')).toEqual('/foobar')
    expect(wrapper.attributes('target')).toEqual('_blank')
    expect(wrapper.attributes('rel')).toEqual('noopener')
    expect(wrapper.classes().length).toBe(0)
  })

  it('should render the given rel to when target==="_blank"', async () => {
    const wrapper = mount(BLink, {
      props: {
        href: '/foobar',
        target: '_blank',
        rel: 'alternate',
      },
    })

    expect(wrapper.element.tagName).toBe('A')
    expect(wrapper.attributes('href')).toEqual('/foobar')
    expect(wrapper.attributes('target')).toEqual('_blank')
    expect(wrapper.attributes('rel')).toEqual('alternate')
    expect(wrapper.classes().length).toBe(0)
  })

  it('should add "active" class when prop active=true', async () => {
    const wrapper = mount(BLink, {
      props: {
        active: true,
      },
    })

    expect(wrapper.element.tagName).toBe('A')
    expect(wrapper.classes()).toContain('active')
    expect(wrapper.classes().length).toBe(1)
  })

  it('should add aria-disabled="true" when disabled', async () => {
    const wrapper = mount(BLink, {
      props: {
        disabled: true,
      },
    })
    expect(wrapper.attributes('aria-disabled')).toBeDefined()
    expect(wrapper.attributes('aria-disabled')).toEqual('true')
  })

  it("should add '.disabled' class when prop disabled=true", async () => {
    const wrapper = mount(BLink, {
      props: {
        disabled: true,
      },
    })
    expect(wrapper.classes()).toContain('disabled')
  })

  it('focus and blur methods work', async () => {
    const wrapper = mount(BLink, {
      attachTo: document.body,
      props: {
        href: '#foobar',
      },
    })

    expect(wrapper.element.tagName).toBe('A')

    expect(document.activeElement).not.toBe(wrapper.element)
    wrapper.vm.focus()
    expect(document.activeElement).toBe(wrapper.element)
    wrapper.vm.blur()
    expect(document.activeElement).not.toBe(wrapper.element)
  })

  describe('click handling', () => {
    it('should invoke click handler bound by Vue when clicked on', async () => {
      let called = 0
      let event = null
      const wrapper = mount(BLink, {
        props: {
          onClick: (e: MouseEvent) => {
            event = e
            called++
          },
        },
      })
      expect(wrapper.element.tagName).toBe('A')
      expect(called).toBe(0)
      expect(event).toEqual(null)
      await wrapper.find('a').trigger('click')
      expect(called).toBe(1)
      expect(event).toBeInstanceOf(MouseEvent)
    })

    it('should NOT invoke click handler bound by Vue when disabled and clicked', async () => {
      let called = 0
      let event = null
      const wrapper = mount(BLink, {
        props: {
          disabled: true,
          onClick: (e: MouseEvent) => {
            event = e
            called++
          },
        },
      })
      expect(wrapper.element.tagName).toBe('A')
      expect(called).toBe(0)
      expect(event).toEqual(null)
      await wrapper.find('a').trigger('click')
      expect(called).toBe(0)
      expect(event).toEqual(null)
    })

    it('should NOT invoke click handler bound via "addEventListener" when disabled and clicked', async () => {
      const wrapper = mount(BLink, {
        props: {
          disabled: true,
        },
      })
      const spy = vi.fn()
      expect(wrapper.element.tagName).toBe('A')
      wrapper.find('a').element.addEventListener('click', spy)
      await wrapper.find('a').trigger('click')
      expect(spy).not.toHaveBeenCalled()
    })

    it('should emit "bv::link::clicked" on event hub when clicked on', async () => {
      const spy = vi.fn()
      const App = {
        components: { BLink },
        template: '<div><b-link href="/foo">link</b-link></div>',
      }

      const wrapper = mount(App)
      expect(wrapper.vm).toBeDefined()
      defaultEventHub.on('bv::link::clicked', spy)

      await wrapper.find('a').trigger('click')
      expect(spy).toHaveBeenCalled()
    })

    it('should not emit "bv::link::clicked" on event hub when clicked on when disabled', async () => {
      const spy = vi.fn()
      const App = {
        components: { BLink },
        template: '<div><b-link href="/foo" :disabled="true">link</b-link></div>',
      }

      const wrapper = mount(App)
      expect(wrapper.vm).toBeDefined()
      defaultEventHub.on('bv::link::clicked', spy)

      await wrapper.find('a').trigger('click')
      expect(spy).not.toHaveBeenCalled()
    })

    it('should emit "clicked::link" on event hub when clicked on', async () => {
      const spy = vi.fn()
      const App = {
        components: { BLink },
        template: '<div><b-link href="/foo">link</b-link></div>',
      }

      const wrapper = mount(App)
      expect(wrapper.vm).toBeDefined()
      defaultEventHub.on('clicked::link', spy)

      await wrapper.find('a').trigger('click')
      expect(spy).toHaveBeenCalled()
    })

    it('should not emit "clicked::link" on event hub when clicked on when disabled', async () => {
      const spy = vi.fn()
      const App = {
        components: { BLink },
        template: '<div><b-link href="/foo" :disabled="true">link</b-link></div>',
      }

      const wrapper = mount(App)
      expect(wrapper.vm).toBeDefined()
      defaultEventHub.on('clicked::link', spy)

      await wrapper.find('a').trigger('click')
      expect(spy).not.toHaveBeenCalled()
    })
  })
})
