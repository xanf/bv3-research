import { ComputedRef, ExtractPropTypes, provide, computed, inject } from 'vue'
import { props } from './useSharedTableProps'
import {
  StylingContext as StylingSimpleContext,
  useTableStyling as useTableSimpleStyling,
} from './useTableSimpleStyling'

const TABLE_CONTEXT_SYMBOL = Symbol('bv-table-styling-context')

interface StylingContext extends StylingSimpleContext {
  headVariant: ComputedRef<string>
  footVariant: ComputedRef<string>
  hasCaption: ComputedRef<boolean>
  captionId: ComputedRef<string | null>
}

export const useTableStyling = (cmpProps: ExtractPropTypes<typeof props>) => {
  const simpleStyling = useTableSimpleStyling(cmpProps)

  const headVariant = computed(() => cmpProps.headVariant)
  const footVariant = computed(() => cmpProps.footVariant)

  const captionId = computed(() =>
    simpleStyling.isStacked.value ? simpleStyling.safeId.value('_caption_') : null
  )
  const hasCaption = computed(() => Boolean(cmpProps.caption || cmpProps.captionHtml))

  const stylingContext: StylingContext = {
    ...simpleStyling,

    headVariant,
    footVariant,

    hasCaption,
    captionId,
  }

  provide(TABLE_CONTEXT_SYMBOL, stylingContext)

  return stylingContext
}

export const useTableStylingContext = () => {
  const context = inject<StylingContext>(TABLE_CONTEXT_SYMBOL)
  if (!context) {
    throw new Error('Used outside of a <b-table>')
  }
  return context
}
