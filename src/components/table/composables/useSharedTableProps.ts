import { computed, ExtractPropTypes, inject, PropType, provide, Ref, toRefs } from 'vue'

import { MaybeCallable, VueCSSClassValue } from '../../../../types'
import { props as tableSimpleStylingProps } from './useTableSimpleStyling'

const TABLE_SHARED_PROPS_CONTEXT_SYMBOL = Symbol('bv-table-shared-props-context')

// +             "prop": "apiUrl",
// +             "prop": "busy",

// +             "prop": "emptyFilteredHtml",
// +             "prop": "emptyFilteredText",
// +             "prop": "emptyHtml",
// +             "prop": "emptyText",

// +             "prop": "filter",
// +             "prop": "filterDebounce",
// +             "prop": "filterFunction",
// +             "prop": "filterIgnoredFields",
// +             "prop": "filterIncludedFields",

// +            "prop": "labelSortAsc",
// +            "prop": "labelSortClear",
// +            "prop": "labelSortDesc",

// +            "prop": "noFooterSorting",
// +            "prop": "noLocalSorting",
// +            "prop": "noProviderFiltering",
// +            "prop": "noProviderPaging",
// +            "prop": "noProviderSorting",

// +            "prop": "noSelectOnClick",

// +            "prop": "noSortReset",
// +            "prop": "perPage",

// +            "prop": "selectMode",
// +            "prop": "selectable",
// +            "prop": "selectedVariant",
// +            "prop": "showEmpty",

// +            "prop": "sortBy",
// +            "prop": "sortCompare",
// +            "prop": "sortCompareLocale",
// +            "prop": "sortCompareOptions",
// +            "prop": "sortDesc",
// +            "prop": "sortDirection",
// +            "prop": "sortIconLeft",
// +            "prop": "sortNullLast",

const callableOrValueProps = {
  tbodyTrAttr: {
    type: [Object, Function] as PropType<MaybeCallable<Record<string, unknown>>>,
    default: () => ({}),
  },
  tbodyTrClass: {
    type: [String, Array, Object, Function] as PropType<MaybeCallable<VueCSSClassValue>>,
    default: '',
  },
}
type CallableOrValuePropsType = ExtractPropTypes<typeof callableOrValueProps>

// "prop": "footClone",
const stylingProps = {
  ...tableSimpleStylingProps,

  headRowVariant: { type: String, default: '' },
  headVariant: { type: String as PropType<string>, default: '' },

  footRowVariant: { type: String, default: '' },
  footVariant: { type: String as PropType<string>, default: '' },

  // thead
  theadClass: { type: [String, Array, Object] as PropType<VueCSSClassValue>, default: '' },
  theadTrClass: { type: [String, Array, Object] as PropType<VueCSSClassValue>, default: '' },

  tbodyClass: { type: [String, Array, Object] as PropType<VueCSSClassValue>, default: '' },
  tbodyTransitionProps: { type: Object, default: null },
  tbodyTransitionHandlers: { type: Object as PropType<Record<string, unknown>>, default: null },

  // tfoot
  // "prop": "footClone",
  tfootClass: { type: [String, Array, Object] as PropType<VueCSSClassValue>, default: '' },
  tfootTrClass: { type: [String, Array, Object] as PropType<VueCSSClassValue>, default: '' },

  // details
  detailsTdClass: { type: [String, Array, Object] as PropType<VueCSSClassValue>, default: '' },

  // caption
  caption: { type: String, default: '' },
  captionHtml: { type: String, default: '' },
  captionTop: { type: Boolean, default: false },
}

type StylingPropsType = ExtractPropTypes<typeof stylingProps>

export const props = {
  ...stylingProps,
  ...callableOrValueProps,
}

type TableLitePropsContext = {
  [Key in keyof StylingPropsType]: Ref<StylingPropsType[Key]>
} & {
  [Key in keyof CallableOrValuePropsType]: Ref<
    CallableOrValuePropsType[Key] extends MaybeCallable<infer V, infer Args>
      ? (...args: Args) => V
      : never
  >
}

const toCallable = <T extends MaybeCallable<unknown>>(
  value: T
): T extends MaybeCallable<infer P, infer Args> ? (...args: Args) => P : never => {
  return (typeof value === 'function' ? value : () => value) as T extends MaybeCallable<
    infer P,
    infer Args
  >
    ? (...args: Args) => P
    : never
}

export const useSharedTableProps = (
  cmpProps: ExtractPropTypes<typeof props>
): TableLitePropsContext => {
  const { tbodyTrAttr, tbodyTrClass, ...rest } = toRefs(cmpProps)
  const context = {
    ...rest,
    tbodyTrAttr: computed(() => toCallable(tbodyTrAttr.value)),
    tbodyTrClass: computed(() => toCallable(tbodyTrClass.value)),
  }
  provide(TABLE_SHARED_PROPS_CONTEXT_SYMBOL, context)

  return context
}

export const useSharedTablePropsContext = (): TableLitePropsContext => {
  const context = inject<TableLitePropsContext>(TABLE_SHARED_PROPS_CONTEXT_SYMBOL)

  if (!context) {
    throw new Error('useSharedTablePropsContext must be used within a TableLite component')
  }

  return context
}
