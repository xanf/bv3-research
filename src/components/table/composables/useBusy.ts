import { provide, inject, computed, ExtractPropTypes, ref, ComputedRef, watch } from 'vue'

import { props as tableProps } from '../BTable.props'
import { functionOrValue } from '../utils/functionOrValue'
import { SLOT_NAME_TABLE_BUSY } from '../../../constants/slots'

const TABLE_BUSY_CONTEXT_SYMBOL = Symbol('bv-table-busy-context')

export interface BusyContext {
  isBusy: ComputedRef<boolean>
  setBusy: (value: boolean) => void
  busyTrProps: ComputedRef<Record<string, unknown>>
}

export const props = {
  busy: { type: Boolean },
}

type BusyEventsEmit = {
  (e: 'update:busy', value: boolean): void
}

export const useBusy = (cmpProps: ExtractPropTypes<typeof tableProps>, emit: BusyEventsEmit) => {
  const localBusy = ref(false)

  const busyTrProps = computed(() => ({
    ...functionOrValue(cmpProps.tbodyTrAttr, null, SLOT_NAME_TABLE_BUSY),
    class: functionOrValue(cmpProps.tbodyTrClass, null, SLOT_NAME_TABLE_BUSY),
  }))

  const context: BusyContext = {
    isBusy: computed(() => cmpProps.busy || localBusy.value),
    setBusy: (value: boolean) => {
      localBusy.value = value
    },
    busyTrProps,
  }

  watch(localBusy, (value) => {
    emit('update:busy', value)
  })

  provide(TABLE_BUSY_CONTEXT_SYMBOL, context)

  return context
}

const defaultContext: BusyContext = {
  isBusy: computed(() => false),
  setBusy: () => ({}),
  busyTrProps: computed(() => ({})),
}

export const useBusyContext = (): BusyContext => inject(TABLE_BUSY_CONTEXT_SYMBOL, defaultContext)
