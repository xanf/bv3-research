import { provide, inject, ComputedRef } from 'vue'

const TABLE_ROW_GROUP_CONTEXT_SYMBOL = Symbol('b-table-row-group-context')

interface TableRowGroupContext {
  isStickyHeader: ComputedRef<string | boolean>
  rowGroupVariant: ComputedRef<string | undefined | null>
  tablePart: 'thead' | 'tbody' | 'tfoot'
}

export const provideTableRowGroupContext = ({
  isStickyHeader,
  rowGroupVariant,
}: TableRowGroupContext) => {
  provide(TABLE_ROW_GROUP_CONTEXT_SYMBOL, {
    isStickyHeader,
    rowGroupVariant,
  })
}

export const useTableRowGroupContext = () => {
  const context = inject<TableRowGroupContext>(TABLE_ROW_GROUP_CONTEXT_SYMBOL)
  if (!context) {
    throw new Error('used outside b-table row group')
  }

  return context
}
