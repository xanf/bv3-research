import { computed, ComputedRef, ref } from '@vue/reactivity'
import { ExtractPropTypes, inject, PropType, provide, Ref, watch } from 'vue'

const TABLE_ITEMS_CONTEXT_SYMBOL = Symbol('bv-table-items-context')

export const props = {
  items: { type: Array as PropType<Record<string, unknown>[] | null>, default: () => [] },
  primaryKey: { type: String as PropType<string | null>, default: null },
}

type ItemsContext = {
  items: Ref<Record<string, unknown>[]>
  primaryKey: ComputedRef<string | null>
}

export const useItems = (
  cmpProps: ExtractPropTypes<typeof props>,
  items: Ref<Record<string, unknown>[]>
): ItemsContext => {
  const localItems = ref(items.value)

  watch(items, (newValue) => (localItems.value = newValue))

  const context: ItemsContext = {
    primaryKey: computed(() => cmpProps.primaryKey),
    items: localItems,
  }

  provide(TABLE_ITEMS_CONTEXT_SYMBOL, context)
  return context
}

export const useItemsContext = (): ItemsContext => {
  const context = inject<ItemsContext>(TABLE_ITEMS_CONTEXT_SYMBOL)

  if (!context) {
    throw new Error('useItemsContext should be used in table context')
  }

  return context
}
