import { provide, inject, ComputedRef } from 'vue'

const TABLE_ROW_CONTEXT_SYMBOL = Symbol('b-table-row-context')

interface TableRowContext {
  rowVariant: ComputedRef<string | undefined>
}

export const provideTableRowContext = (context: TableRowContext) => {
  provide(TABLE_ROW_CONTEXT_SYMBOL, context)
}

export const useTableRowContext = () => {
  const context = inject<TableRowContext>(TABLE_ROW_CONTEXT_SYMBOL)
  if (!context) {
    throw new Error('used outside b-tr')
  }

  return context
}
