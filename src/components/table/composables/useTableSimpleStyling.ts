import {
  ComputedRef,
  ExtractPropTypes,
  toRefs,
  provide,
  computed,
  useAttrs,
  inject,
  PropType,
} from 'vue'
import { VueCSSClassValue } from '../../../../types'
import { useComputedBooleanAttribute } from '../../../composables/useComputedBooleanAttribute'
import { useSafeId } from '../../../composables/useSafeId'

export const props = {
  bordered: { type: Boolean, default: false },
  borderless: { type: Boolean, default: false },
  captionTop: { type: Boolean, default: false },
  dark: { type: Boolean, default: false },
  fixed: { type: Boolean, default: false },
  hover: { type: Boolean, default: false },
  noBorderCollapse: { type: Boolean, default: false },
  outlined: { type: Boolean, default: false },
  responsive: { type: [Boolean, String], default: false },
  small: { type: Boolean, default: false },
  stacked: { type: [Boolean, String], default: false },

  // If a string, it is assumed to be the table `max-height` value
  stickyHeader: { type: [Boolean, String], default: false },
  striped: { type: Boolean, default: false },
  tableClass: { type: [String, Array, Object] as PropType<VueCSSClassValue>, default: '' },
  tableVariant: { type: String, default: '' },
}

const TABLE_CONTEXT_SYMBOL = Symbol('bv-table-simple-styling-context')

export interface StylingContext {
  isDark: ComputedRef<boolean>
  isStacked: ComputedRef<boolean | string>
  isResponsive: ComputedRef<boolean | string>
  hasStickyHeader: ComputedRef<boolean | string>

  tableVariant: ComputedRef<string | undefined>

  safeId: ComputedRef<(suffix?: string) => string | null>

  wrapperClasses: ComputedRef<VueCSSClassValue>
  wrapperStyles: ComputedRef<Record<string, unknown>>
  tableClasses: ComputedRef<VueCSSClassValue>
  tableAttrs: ComputedRef<Record<string, unknown>>
}

type TableRendererCmpPropsType = ExtractPropTypes<typeof props>

export const useTableStyling = (cmpProps: TableRendererCmpPropsType) => {
  const { stacked, responsive, stickyHeader } = toRefs(cmpProps)

  const isDark = computed(() => cmpProps.dark)
  const isStacked = useComputedBooleanAttribute(stacked)
  const isResponsive = useComputedBooleanAttribute(responsive)

  const normalizedStickyHeader = useComputedBooleanAttribute(stickyHeader)
  const hasStickyHeader = computed(() => {
    return cmpProps.stacked ? false : normalizedStickyHeader.value
  })

  const tableVariant = computed(() => cmpProps.tableVariant)
  const { safeId } = useSafeId(cmpProps)

  const wrapperClasses = computed(() => ({
    'b-table-sticky-header': Boolean(hasStickyHeader.value),
    'table-responsive': isResponsive.value === true,
    [`table-responsive-${cmpProps.responsive}`]:
      isResponsive.value !== true && Boolean(isResponsive.value),
  }))

  const wrapperStyles = computed(() =>
    typeof hasStickyHeader.value === 'string' ? { maxHeight: hasStickyHeader.value } : {}
  )

  const tableClasses = computed(() => [
    cmpProps.tableClass,
    {
      'table-striped': cmpProps.striped,
      'table-hover': cmpProps.hover,
      'table-dark': cmpProps.dark,
      'table-bordered': cmpProps.bordered,
      'table-borderless': cmpProps.borderless,
      'table-sm': cmpProps.small,
      // The following are b-table custom styles
      border: cmpProps.outlined,
      'b-table-fixed': cmpProps.fixed,
      'b-table-caption-top': cmpProps.captionTop,
      'b-table-no-border-collapse': cmpProps.noBorderCollapse,
      [`${cmpProps.dark ? 'bg' : 'table'}-${cmpProps.tableVariant}`]: Boolean(
        cmpProps.tableVariant
      ),
    },
  ])

  const tableAttrs = computed(() => ({
    id: safeId.value(),
    role: useAttrs()['role'] || 'table',
  }))

  const stylingContext: StylingContext = {
    safeId,
    isDark,
    isStacked,
    isResponsive,
    hasStickyHeader,

    tableVariant,

    wrapperClasses,
    wrapperStyles,
    tableClasses,
    tableAttrs,
  }

  provide(TABLE_CONTEXT_SYMBOL, stylingContext)

  return {
    ...stylingContext,

    isResponsive,
    isStickyHeader: hasStickyHeader,
  }
}

export const useTableStylingContext = () => {
  const context = inject<StylingContext>(TABLE_CONTEXT_SYMBOL)
  if (!context) {
    throw new Error('Used outside of a <b-table>')
  }
  return context
}
