const camelizeRE = /-(\w)/g

export const propNameFromEvent = (eventName: string) =>
  `on-${eventName}`.replace(camelizeRE, (_, c) => (c ? c.toUpperCase() : ''))
