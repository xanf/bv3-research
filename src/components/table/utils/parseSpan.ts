import { toInteger } from '../../../utils/number'

// Parse a rowspan or colspan into a digit (or `null` if < `1` )
export const parseSpan = (value: number | string | undefined) => {
  if (value === undefined) {
    return null
  }
  value = toInteger(value, 0)
  return value > 0 ? value : null
}
