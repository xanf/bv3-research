import {
  TdAttrFieldFunction,
  TdClassFieldFunction,
  ThAttrFieldFunction,
  VueCSSClassValue,
} from '../../../../types'
import { clone } from '../../../utils/object'
import { startCase } from '../../../utils/string'
import { IGNORED_FIELD_KEYS } from './constants'

export type FormatterFn = (
  value: unknown,
  key: string,
  item: object | null,
  type: 'head' | 'foot' | NormalizedField
) => string

export type NormalizedField = {
  key: string
  label: string
  labelHtml?: string

  headerTitle?: string
  headerAbbr?: string
  class?: VueCSSClassValue
  formatter?: FormatterFn | string
  sortable?: boolean
  sortKey?: string
  sortDirection?: string
  sortByFormatter?: boolean | (() => boolean)
  filterByFormatted?: boolean | (() => boolean)

  tdClass?: VueCSSClassValue | TdClassFieldFunction
  thClass?: VueCSSClassValue
  thStyle?: Record<string, string>
  variant?: string

  tdAttr?: Record<string, unknown> | TdAttrFieldFunction
  thAttr?: Record<string, unknown> | ThAttrFieldFunction
  isRowHeader?: boolean
  stickyColumn?: boolean
}

export type FieldValue =
  | string
  | FormatterFn
  | (Omit<NormalizedField, 'key' | 'label'> & { key?: string; label?: string })
  | boolean
  | null
  | undefined

// Private function to massage field entry into common object format
const processField = (key: string, value: FieldValue): NormalizedField | null => {
  // Label shortcut
  if (typeof value === 'string') {
    return { key, label: value }
  }

  // Formatter shortcut
  if (typeof value === 'function') {
    return { key, formatter: value, label: startCase(key) }
  }

  if (value && typeof value === 'object') {
    return { ...clone(value), key: 'key' in value ? value.key ?? key : key }
  }

  // Fallback to just key + label
  if (value !== false) {
    /* istanbul ignore next */
    return { key, label: startCase(key) }
  }

  return null
}

// We normalize fields into an array of objects
// [ { key:..., label:..., ...}, {...}, ..., {..}]
export const normalizeFields = (
  origFields: FieldValue[] | null | undefined,
  items: Record<string, unknown>[] = []
): NormalizedField[] => {
  const fields: NormalizedField[] = []

  if (Array.isArray(origFields)) {
    // Normalize array Form
    origFields.filter(Boolean).forEach((f) => {
      if (typeof f === 'string') {
        fields.push({ key: f, label: startCase(f) })
      } else if (f && typeof f === 'object' && 'key' in f && typeof f.key === 'string') {
        // Full object definition. We use assign so that we don't mutate the original
        fields.push(clone(f))
      } else if (f && typeof f === 'object' && Object.keys(f).length === 1) {
        // Shortcut object (i.e. { 'foo_bar': 'This is Foo Bar' }
        const [[key, value]] = Object.entries(f)
        const field = processField(key, value as string)
        if (field) {
          fields.push(field)
        }
      }
    })
  }

  // If no field provided, take a sample from first record (if exits)
  if (fields.length === 0 && Array.isArray(items) && items.length > 0) {
    const sample = items[0]
    Object.keys(sample).forEach((k) => {
      if (!IGNORED_FIELD_KEYS[k]) {
        fields.push({ key: k, label: startCase(k) })
      }
    })
  }

  const memo: Set<string> = new Set()

  return fields
    .filter((f) => {
      if (!f || memo.has(f.key)) {
        return false
      }

      memo.add(f.key)
      return true
    })
    .map((f) => ({
      ...f,
      label: typeof f.label === 'string' ? f.label : startCase(f.key),
    }))
}
