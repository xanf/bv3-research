import { props as tableSimpleProps } from './BTableSimple.props'
import { props as fieldProps } from './composables/useFields'
import { props as tableEventsProps } from './composables/useTableEvents'
import { props as itemsProps } from './composables/useItems'
import { props as sharedTableProps } from './composables/useSharedTableProps'

export const props = {
  ...tableSimpleProps,

  ...sharedTableProps,

  ...fieldProps,
  ...itemsProps,

  ...tableEventsProps,
}
