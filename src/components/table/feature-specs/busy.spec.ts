import { it, describe, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import BTable from '../BTable.vue'
import { nextTick } from 'vue'
import { findContextBySymbolName } from '../../../../tests/utils/findContextBySymbolName'
import { BusyContext } from '../composables/useBusy'

const testItems = [
  { a: 1, b: 2, c: 3 },
  { a: 5, b: 5, c: 6 },
  { a: 7, b: 8, c: 9 },
]

describe('table > busy state', () => {
  it('default should have attribute aria-busy=false', async () => {
    const wrapper = mount(BTable, {
      props: {
        items: testItems,
      },
    })
    const table = wrapper.find('table')
    expect(table.attributes('aria-busy')).toBeDefined()
    expect(table.attributes('aria-busy')).toEqual('false')
  })

  it('default should have item rows rendered', async () => {
    const wrapper = mount(BTable, {
      props: {
        items: testItems,
      },
    })

    const table = wrapper.find('table')
    expect(table.find('tbody').exists()).toBe(true)
    expect(
      table
        .find('tbody')
        .findAll('tr')
        .every((w) => w.exists())
    ).toBe(true)

    expect(table.find('tbody').findAll('tr').length).toBe(testItems.length)
  })

  it('should have attribute aria-busy=true when prop busy=true', async () => {
    const wrapper = mount(BTable, {
      props: {
        busy: true,
        items: testItems,
      },
    })
    const table = wrapper.find('table')

    expect(table.attributes('aria-busy')).toBeDefined()
    expect(table.attributes('aria-busy')).toEqual('true')
  })

  it('should have attribute aria-busy=true when set to busy internally', async () => {
    const wrapper = mount(BTable, {
      props: {
        items: testItems,
      },
    })
    const table = wrapper.find('table')

    const setBusy = findContextBySymbolName<BusyContext>(
      wrapper.vm.$,
      'bv-table-busy-context'
    )!.setBusy

    expect(table.attributes('aria-busy')).toBeDefined()
    expect(table.attributes('aria-busy')).toEqual('false')

    setBusy(true)
    await nextTick()

    expect(table.attributes('aria-busy')).toBeDefined()
    expect(table.attributes('aria-busy')).toEqual('true')
  })

  it('should emit update:busy event when busy is changed internally', async () => {
    const wrapper = mount(BTable, {
      props: {
        items: testItems,
      },
    })

    const setBusy = findContextBySymbolName<BusyContext>(
      wrapper.vm.$,
      'bv-table-busy-context'
    )!.setBusy

    expect(wrapper.emitted('update:busy')).toBeUndefined()

    setBusy(true)
    await nextTick()

    expect(wrapper.emitted('update:busy')).toBeDefined()
    expect(wrapper.emitted<unknown[]>('update:busy')![0][0]).toEqual(true)
  })

  it('should render table-busy slot when prop busy=true and slot provided', async () => {
    const wrapper = mount(BTable, {
      props: {
        busy: false,
        items: testItems,
      },
      slots: {
        'table-busy': '<span>busy slot content</span>',
      },
    })

    const table = wrapper.find('table')
    expect(table.attributes('aria-busy')).toBeDefined()
    expect(table.attributes('aria-busy')).toEqual('false')
    expect(table.find('tbody').exists()).toBe(true)
    expect(table.find('tbody tr').exists()).toBe(true)

    expect(table.find('tbody').findAll('tr').length).toBe(testItems.length)

    await wrapper.setProps({
      busy: true,
    })

    expect(table.attributes('aria-busy')).toBeDefined()
    expect(table.attributes('aria-busy')).toEqual('true')
    expect(table.find('tbody').exists()).toBe(true)
    expect(table.find('tbody tr').exists()).toBe(true)
    expect(table.find('tbody').findAll('tr').length).toBe(1)
    expect(table.find('tbody').text()).toContain('busy slot content')
    expect(table.find('tbody').find('tr').classes()).toContain('b-table-busy-slot')

    await wrapper.setProps({
      busy: false,
    })

    expect(table.attributes('aria-busy')).toBeDefined()
    expect(table.attributes('aria-busy')).toEqual('false')
    expect(table.find('tbody').exists()).toBe(true)
    expect(wrapper.find('tbody tr').exists()).toBe(true)

    expect(wrapper.find('tbody').findAll('tr').length).toBe(testItems.length)
  })
})
