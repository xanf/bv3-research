import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import BTable from '../BTable.vue'
import { nextTick } from 'vue'
import { findContextBySymbolName } from '../../../../tests/utils/findContextBySymbolName'
import { BusyContext } from '../composables/useBusy'

const testItems = [{ a: 1, b: 2, c: 3 }]
const testFields = [
  { key: 'a', label: 'A' },
  { key: 'b', label: 'B' },
  { key: 'c', label: 'C' },
]

const noop = () => ({})

describe('table > thead events', () => {
  it('should not emit head-clicked event when a head cell is clicked and no head-clicked listener', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
    })
    const $rows = wrapper.findAll('thead > tr')
    expect($rows.length).toBe(1)
    const $ths = wrapper.findAll('thead > tr > th')
    expect($ths.length).toBe(testFields.length)
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
    await $ths[0].trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
    await $ths[1].trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
    await $ths[2].trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
  })

  it('should emit head-clicked event when a head cell is clicked', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onHeadClicked: noop,
      },
    })
    const $rows = wrapper.findAll('thead > tr')
    expect($rows.length).toBe(1)
    const $ths = wrapper.findAll('thead > tr > th')
    expect($ths.length).toBe(testFields.length)
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
    await $ths[0].trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeDefined()
    expect(wrapper.emitted<unknown[]>('head-clicked')!.length).toBe(1)
    expect(wrapper.emitted<unknown[]>('head-clicked')![0][0]).toEqual(testFields[0].key) // Field key
    expect(wrapper.emitted<unknown[]>('head-clicked')![0][1]).toEqual(testFields[0]) // Field definition
    expect(wrapper.emitted<unknown[]>('head-clicked')![0][2]).toBeInstanceOf(MouseEvent) // Event
    expect(wrapper.emitted<unknown[]>('head-clicked')![0][3]).toBe(false) // Is footer

    await $ths[2].trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!.length).toBe(2)
    expect(wrapper.emitted<unknown[]>('head-clicked')![1][0]).toEqual(testFields[2].key) // Field key
    expect(wrapper.emitted<unknown[]>('head-clicked')![1][1]).toEqual(testFields[2]) // Field definition
    expect(wrapper.emitted<unknown[]>('head-clicked')![1][2]).toBeInstanceOf(MouseEvent) // Event
    expect(wrapper.emitted<unknown[]>('head-clicked')![1][3]).toBe(false) // Is footer
  })

  it('should not emit head-clicked event when prop busy is set', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        busy: true,
        onHeadClicked: noop,
      },
    })
    const $ths = wrapper.findAll('thead > tr > th')
    expect($ths.length).toBe(testFields.length)
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
    await $ths[0].trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
  })

  it('should not emit head-clicked event when set to busy internally', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onHeadClicked: noop,
      },
    })

    const setBusy = findContextBySymbolName<BusyContext>(
      wrapper.vm.$,
      'bv-table-busy-context'
    )!.setBusy

    setBusy(true)
    await nextTick()

    const $ths = wrapper.findAll('thead > tr > th')
    expect($ths.length).toBe(testFields.length)
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
    await $ths[0].trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
  })

  it('should not emit head-clicked event when clicking on a button or other interactive element', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
        onHeadClicked: noop,
      },
      slots: {
        'head(a)': { template: '<button id="a">button</button>' },
        'head(b)': { template: '<input id="b">' },
        'head(c)': { template: '<a href="#" id="c">link</a>' },
      },
    })

    const $ths = wrapper.findAll('thead > tr > th')
    expect($ths.length).toBe(testFields.length)
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()

    const $btn = wrapper.find('button[id="a"]')
    expect($btn.exists()).toBe(true)
    await $btn.trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()

    const $input = wrapper.find('input[id="b"]')
    expect($input.exists()).toBe(true)
    await $input.trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()

    const $link = wrapper.find('a[id="c"]')
    expect($link.exists()).toBe(true)
    await $link.trigger('click')
    expect(wrapper.emitted<unknown[]>('head-clicked')!).toBeUndefined()
  })
})
