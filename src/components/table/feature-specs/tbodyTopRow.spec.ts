import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import { NormalizedField, normalizeFields } from '../utils/normalizeFields'
import BTable from '../BTable.vue'
import { h } from 'vue'

const testItems = [
  { a: 1, b: 2, c: 3 },
  { a: 5, b: 5, c: 6 },
  { a: 7, b: 8, c: 9 },
]
const testFields = ['a', 'b', 'c']

describe('table > tbody top-row slot', () => {
  it('should not have top row by default', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
    })
    const table = wrapper.find('table')

    expect(table.find('tbody').exists()).toBe(true)
    expect(table.find('tbody > tr').exists()).toBe(true)
    expect(table.findAll('tbody > tr').length).toBe(testItems.length)
  })

  it('should render named slot `top-row`', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'top-row': `<td span="${testFields.length}">foobar</td>`,
      },
    })
    const table = wrapper.find('table')

    expect(table.find('tbody').exists()).toBe(true)
    expect(table.find('tbody > tr').exists()).toBe(true)
    expect(table.findAll('tbody > tr').length).toBe(testItems.length + 1)
    expect(table.findAll('tbody > tr')[0].text()).toBe('foobar')
    expect(table.findAll('tbody > tr')[0].classes()).toContain('b-table-top-row')
  })

  it('should render scoped slot `top-row`', async () => {
    let fields: NormalizedField[] = []
    let columns
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'top-row': function (scope) {
          fields = scope.fields
          columns = scope.columns
          return h('td', { span: columns }, 'foobar')
        },
      },
    })
    const table = wrapper.find('table')

    expect(table.find('tbody').exists()).toBe(true)
    expect(columns).toBe(3)
    expect(fields).toEqual(normalizeFields(testFields))
    expect(wrapper.find('tbody > tr').exists()).toBe(true)
    expect(wrapper.findAll('tbody > tr').length).toBe(testItems.length + 1)
    expect(wrapper.findAll('tbody > tr')[0].text()).toBe('foobar')
    expect(wrapper.findAll('tbody > tr')[0].classes()).toContain('b-table-top-row')
  })
})
