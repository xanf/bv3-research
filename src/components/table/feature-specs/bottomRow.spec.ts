import { describe, it, expect } from 'vitest'
import { h } from 'vue'
import { mount } from '@vue/test-utils'
import BTable from '../BTable.vue'
import { NormalizedField, normalizeFields } from '../utils/normalizeFields'

const testItems = [
  { a: 1, b: 2, c: 3 },
  { a: 5, b: 5, c: 6 },
  { a: 7, b: 8, c: 9 },
]
const testFields = ['a', 'b', 'c']

describe('table > tbody bottom-row slot', () => {
  it('should not have bottom row by default', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
    })
    expect(wrapper.find('tbody').exists()).toBe(true)
    expect(wrapper.find('tbody > tr').exists()).toBe(true)
    expect(wrapper.findAll('tbody > tr').length).toBe(testItems.length)
  })

  it('should render named slot `bottom-row`', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'bottom-row': `<td span="${testFields.length}">foobar</td>`,
      },
    })

    expect(wrapper.find('tbody').exists()).toBe(true)
    expect(wrapper.find('tbody > tr').exists()).toBe(true)
    expect(wrapper.findAll('tbody > tr').length).toBe(testItems.length + 1)
    expect(wrapper.findAll('tbody > tr')[testItems.length].text()).toBe('foobar')
    expect(wrapper.findAll('tbody > tr')[testItems.length].classes()).toContain(
      'b-table-bottom-row'
    )
  })

  it('should render scoped slot `bottom-row`', async () => {
    let fields: NormalizedField[] = []
    let columns
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'bottom-row': function (scope) {
          fields = scope.fields
          columns = scope.columns
          return h('td', { span: columns }, 'foobar')
        },
      },
    })
    expect(wrapper.find('tbody').exists()).toBe(true)
    expect(columns).toBe(3)
    expect(fields).toEqual(normalizeFields(testFields))
    expect(wrapper.find('tbody > tr').exists()).toBe(true)
    expect(wrapper.findAll('tbody > tr').length).toBe(testItems.length + 1)
    expect(wrapper.findAll('tbody > tr')[testItems.length].text()).toBe('foobar')
    expect(wrapper.findAll('tbody > tr')[testItems.length].classes()).toContain(
      'b-table-bottom-row'
    )
  })
})
