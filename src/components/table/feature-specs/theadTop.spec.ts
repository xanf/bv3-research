import { describe, it, expect } from 'vitest'
import { h } from 'vue'
import { mount } from '@vue/test-utils'
import { NormalizedField, normalizeFields } from '../utils/normalizeFields'
import BTable from '../BTable.vue'

const testItems = [
  { a: 1, b: 2, c: 3 },
  { a: 5, b: 5, c: 6 },
  { a: 7, b: 8, c: 9 },
]
const testFields = ['a', 'b', 'c']

describe('table > thead thead-top slot', () => {
  it('should not have thead-top row by default', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
    })

    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('thead').exists()).toBe(true)
    expect(wrapper.findAll('thead > tr').every((w) => w.exists())).toBe(true)
    expect(wrapper.findAll('thead > tr').length).toBe(1)
  })

  it('should render named slot `thead-top`', async () => {
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'thead-top': `<tr class="test"><th span="${testFields.length}">foobar</th></tr>`,
      },
    })

    expect(wrapper.find('thead').exists()).toBe(true)
    expect(wrapper.findAll('thead > tr').every((w) => w.exists())).toBe(true)
    expect(wrapper.findAll('thead > tr').length).toBe(2)
    expect(wrapper.findAll('thead > tr')[0].text()).toBe('foobar')
    expect(wrapper.findAll('thead > tr')[0].classes()).toContain('test')
  })

  it('should render scoped slot `thead-top`', async () => {
    let fields: NormalizedField[] = []
    let columns
    const wrapper = mount(BTable, {
      props: {
        fields: testFields,
        items: testItems,
      },
      slots: {
        'thead-top': function (scope) {
          fields = scope.fields
          columns = scope.columns
          return h('tr', { class: 'test' }, [h('th', { span: columns }, 'foobar')])
        },
      },
    })

    expect(wrapper.find('table').exists()).toBe(true)
    expect(wrapper.find('thead').exists()).toBe(true)
    expect(columns).toBe(3)
    expect(fields).toEqual(normalizeFields(testFields, []))
    expect(wrapper.findAll('thead > tr').every((w) => w.exists())).toBe(true)
    expect(wrapper.findAll('thead > tr').length).toBe(2)
    expect(wrapper.findAll('thead > tr')[0].text()).toBe('foobar')
    expect(wrapper.findAll('thead > tr')[0].classes()).toContain('test')
  })
})
