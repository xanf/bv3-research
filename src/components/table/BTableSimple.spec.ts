import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import BTableSimple from './BTableSimple.vue'

describe('table-simple', () => {
  it('has expected default classes', async () => {
    const wrapper = mount(BTableSimple)
    const table = wrapper.find('table')

    expect(table.exists()).toBe(true)
    expect(table.classes().sort()).toEqual(['b-table', 'table'])
    expect(table.text()).toBe('')
  })

  it.each([
    ['striped', 'table-striped'],
    ['bordered', 'table-bordered'],
    ['borderless', 'table-borderless'],
    ['hover', 'table-hover'],
    ['small', 'table-sm'],
    ['dark', 'table-dark'],
    ['outlined', 'border'],
    ['fixed', 'b-table-fixed'],
  ])('has class %s when %s=true', (prop, cssClass) => {
    const wrapper = mount(BTableSimple, {
      props: {
        [prop]: true,
      },
    })
    const table = wrapper.find('table')

    expect(table.exists()).toBe(true)
    expect(table.classes().sort()).toEqual(['b-table', 'table', cssClass].sort())
  })

  it('has class "table-responsive" when responsive=true', async () => {
    const wrapper = mount(BTableSimple, {
      props: {
        responsive: true,
      },
    })
    const wrapperDiv = wrapper.find('div')

    expect(wrapperDiv.classes()).toEqual(['table-responsive'])
    expect(wrapperDiv.find('table').classes().sort()).toEqual(['b-table', 'table'])
  })

  it('has class "table-responsive-md" when responsive=md', async () => {
    const wrapper = mount(BTableSimple, {
      props: {
        responsive: 'md',
      },
    })
    const wrapperDiv = wrapper.find('div')
    expect(wrapperDiv.classes()).toEqual(['table-responsive-md'])
    expect(wrapperDiv.find('table').classes().sort()).toEqual(['b-table', 'table'])
  })

  it('renders content from default slot', async () => {
    const wrapper = mount(BTableSimple, {
      slots: {
        default: 'foobar',
      },
    })
    const table = wrapper.find('table')

    expect(table.exists()).toBe(true)
    expect(table.classes().sort()).toEqual(['b-table', 'table'])
    expect(table.text()).toContain('foobar')
  })
})
