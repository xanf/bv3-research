import { IS_BROWSER } from '../constants/env'
import { getNoWarn } from './env'

/**
 * Log a warning message to the console with BootstrapVue formatting
 * @param {string} message
 */
export const warn = (message: string, source: string | null = null) => /* istanbul ignore next */ {
  if (!getNoWarn()) {
    console.warn(`[BootstrapVue warn]: ${source ? `${source} - ` : ''}${message}`)
  }
}

/**
 * Warn when no Promise support is given
 * @param {string} source
 * @returns {boolean} warned
 */
export const warnNotClient = (source: string) => {
  /* istanbul ignore else */
  if (IS_BROWSER) {
    return false
  } else {
    warn(`${source}: Can not be called during SSR.`)
    return true
  }
}
