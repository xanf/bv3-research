import { isPlainObject } from './inspect'

export const cloneDeep = <T>(obj: T, defaultValue = obj): T => {
  if (Array.isArray(obj)) {
    return obj.reduce((result, val) => [...result, cloneDeep(val, val)], [])
  }
  if (isPlainObject(obj)) {
    return Object.keys(obj).reduce(
      (result, key) => ({ ...result, [key]: cloneDeep(obj[key], obj[key]) }),
      {} as T
    )
  }
  return defaultValue
}
