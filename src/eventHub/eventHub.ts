import mitt, { Emitter } from 'mitt'

export const EVENT_HUB_SYMBOL = Symbol('eventHub')

export class EventHub {
  // TODO: improve typing of possible events here
  emitter: Emitter<Record<string, unknown>>

  constructor() {
    this.emitter = mitt()
  }

  on = (event: string, handler: (...args: unknown[]) => void) => {
    this.emitter.on(event, handler)
  }

  off = (event: string, handler: (...args: unknown[]) => void) => {
    this.emitter.off(event, handler)
  }

  emit = (event: string, payload: unknown) => {
    this.emitter.emit(event, payload)
  }
}

export const defaultEventHub = new EventHub()
