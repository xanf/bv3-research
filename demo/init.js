import Vue from 'vue'
import 'bootstrap/scss/bootstrap.scss'

// postponing imports after compat config was set
async function run() {
  const { default: BootstrapVue, BootstrapVueIcons } = await import('bootstrap-vue/src')
  const { default: Root } = await import('./root.vue')
  const PortalVue = await import('portal-vue')
  PortalVue.Portal.options.compatConfig = {
    MODE: 2,
    RENDER_FUNCTION: 'suppress-warning',
    INSTANCE_SCOPED_SLOTS: 'suppress-warning',
    OPTIONS_BEFORE_DESTROY: 'suppress-warning',
  }
  PortalVue.PortalTarget.options.compatConfig = {
    MODE: 2,
    RENDER_FUNCTION: 'suppress-warning',
    WATCH_ARRAY: 'suppress-warning',
    OPTIONS_BEFORE_DESTROY: 'suppress-warning',
    INSTANCE_SCOPED_SLOTS: 'suppress-warning',
  }
  PortalVue.Wormhole.$.type.compatConfig = {
    MODE: 3,
    INSTANCE_SET: 'suppress-warning',
    INSTANCE_DELETE: 'suppress-warning',
  }

  Vue.use(BootstrapVue)
  Vue.use(BootstrapVueIcons)

  const app = new Vue({
    el: '#app',
    render: (h) => h(Root),
  })
}

run()
