export type VueCSSClassValue = string | Array<VueCSSClassValue> | Record<string, boolean>
export type MaybeCallable<T, Args extends Array<unknown> = Array<unknown>> =
  | T
  | ((...args: Args) => T)

export type TdClassFieldFunction = <
  T = unknown,
  Item extends Record<string, unknown> = Record<string, unknown>
>(
  value: T,
  key: string,
  item: Item
) => VueCSSClassValue | undefined

export type TdAttrFieldFunction = <
  T = unknown,
  Item extends Record<string, unknown> = Record<string, unknown>
>(
  value: T,
  key: string,
  item: Item
) => Record<string, unknown>

export type ThAttrFieldFunction = <
  T = unknown,
  Item extends Record<string, unknown> = Record<string, unknown>
>(
  value: T,
  key: string,
  item: Item | null,
  type: string
) => Record<string, unknown>

export type FormatterFunction = <
  T = unknown,
  Item extends Record<string, unknown> = Record<string, unknown>
>(
  value: T,
  key: string,
  item: Item
) => string
